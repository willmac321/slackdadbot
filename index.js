const slackEventsApi = require('@slack/events-api');
const SlackClient = require('@slack/client').WebClient;
const express = require('express');

// *** Initialize an Express application
const app = express();

// *** Initialize a client with your access token
const slack = new SlackClient(process.env.SLACK_ACCESS_TOKEN);

// *** Initialize event adapter using signing secret from environment variables ***
const slackEvents = slackEventsApi.createEventAdapter(process.env.SLACK_SIGNING_SECRET);

var selectDadJoke = ["What time did the man go to the dentist? Tooth hurt-y.","My dad literally told me this one last week: 'Did you hear about the guy who invented Lifesavers? They say he made a mint.'", "A ham sandwich walks into a bar and orders a beer. Bartender says, 'Sorry we don't serve food here.'","Whenever the cashier at the grocery store asks my dad if he would like the milk in a bag he replies, 'No, just leave it in the carton!'","Why do chicken coops only have two doors? Because if they had four, they would be chicken sedans!","Why did the Clydesdale give the pony a glass of water? Because he was a little horse!","How do you make a Kleenex dance? Put a little boogie in it!","Two peanuts were walking down the street. One was a salted.","I used to have a job at a calendar factory but I got the sack because I took a couple of days off.","How do you make holy water? You boil the hell out of it.","Two guys walk into a bar, the third one ducks.","A woman is on trial for beating her husband to death with his guitar collection. Judge says, 'First offender?' She says, 'No, first a Gibson! Then a Fender!'","Anytime I do something smart my dad says, 'Wow, you're a fart smella...I mean smart fella!'","I had a dream that I was a muffler last night. I woke up exhausted!","How do you tell the difference between a frog and a horny toad? A frog says, 'Ribbit, ribbit' and a horny toad says, 'Rub it, rub it.'","5/4 of people admit that they’re bad with fractions.","You can't plant flowers if you haven't botany.","What's the difference between a good joke and a bad joke timing.","I was just looking at my ceiling. Not sure if it’s the best ceiling in the world, but it’s definitely up there.","I used to have a job collecting leaves. I was raking it in.","Shout out to my grandma, that's the only way she can hear.","Why aren't jet skis called boatercyles?","Toasters were the first form of pop-up notifications.","I've been addicted to cold turkey for 2 years. I keep telling people I'm trying to quit cold turkey but nobody is taking me seriously.","I just read a book about Stockholm syndrome. It was pretty bad at first, but by the end I liked it.","What's the difference between a hippo and a zippo? One is really heavy, the other is a little lighter."];
function getDadJoke(){
	//Get a random dad joke
	 var min=0;
	 var max=selectDadJoke.length-1;
	 var randInt= Math.floor(Math.random()* (max - min)) + min;
	 return selectDadJoke[randInt];
}

// *** Plug the event adapter into the express app as middleware ***
app.use('/slack/events', slackEvents.expressMiddleware());

// *** Attach listeners to the event adapter ***

// *** Greeting any user that says "hi" ***
slackEvents.on('app_mention', (message) => {
  console.log(message);
  // Only deal with messages that have no subtype (plain messages) and contain 'hi'
  if (!message.subtype && /hi/i.test(message.text)) {
    
    // Respond to the message back in the same channel
    slack.chat.postMessage({ channel: message.channel, text: `Hey <@${message.user}>! :tada:` })
      .catch(console.error);
  }
});

// *** Responding to reactions with the same emoji ***
slackEvents.on('message', (message) => {
  var rv = '';
  var isFound = false;
  if (!message.subtype){
    var m = message.text.toLowerCase();
    if (m.indexOf("i'm ")>-1)
      {
        var str = ""
  			var i=m.indexOf("i'm ");
  			var myStr=(m.substring(i+4)).split(" ");
  			i=0;
  			while (myStr[i]==""||myStr[i]=="a" || myStr[i]=="the" || myStr[i]=="an"){
  				i++;
  			}
  			var test=myStr[i].toLowerCase();
  			rv = `Hi ${myStr[i]}, I'm dadBot! Pleased to meet you!`;
        isFound = true;
      }    
    else if(m.indexOf("im ")>-1)
      {
        var str = ""
        var i=m.indexOf("im ");
        var myStr=(m.substring(i+3)).split(" ");
        i=0;
        while (myStr[i]==""||myStr[i]=="a" || myStr[i]=="the" || myStr[i]=="an"){
          i++;
        }
        var test=myStr[i].toLowerCase();
        rv = `Hi ${myStr[i]}, I'm dadBot! Pleased to meet you!`;
        isFound = true;
      }
    else if(m.indexOf("!dadjoke")>-1){
	 //Get a random dad joke
			rv=getDadJoke();
      isFound = true;
    }
  // Respond to the reaction back with the same emoji
    if(isFound){
       slack.chat.postMessage({ channel: message.channel, text: `${rv}` })
        .catch(console.error);
    }
  }
});

// *** Handle errors ***
slackEvents.on('error', (error) => {
  if (error.code === slackEventsApi.errorCodes.TOKEN_VERIFICATION_FAILURE) {
    // This error type also has a `body` propery containing the request body which failed verification.
    console.error(`An unverified request was sent to the Slack events Request URL. Request body: \
${JSON.stringify(error.body)}`);
  } else {
    console.error(`An error occurred while handling a Slack event: ${error.message}`);
  }
});

// Start the express application
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`server listening on port ${port}`);
});


